/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testcase;

/**
 *
 * @author Snow
 */
class OX {

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (table[0][0].equals(currentPlayer) && table[0][1].equals(currentPlayer) && table[0][2].equals(currentPlayer)) {
            return true;
        } else if (table[1][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[1][2].equals(currentPlayer)) {
            return true;
        } else if (table[2][0].equals(currentPlayer) && table[2][1].equals(currentPlayer) && table[2][2].equals(currentPlayer)) {
            return true;
        } else if (table[0][0].equals(currentPlayer) && table[1][0].equals(currentPlayer) && table[2][0].equals(currentPlayer)) {
            return true;
        } else if (table[0][1].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][1].equals(currentPlayer)) {
            return true;
        } else if (table[0][2].equals(currentPlayer) && table[1][2].equals(currentPlayer) && table[2][2].equals(currentPlayer)) {
            return true;
        } else if (table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer)) {
            return true;
        } else if (table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer)) {
            return true;
        }
        return false;
    }

    static boolean checkDraw(String[][] table, String currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (table.equals(currentPlayer)) {
                    return false;
                }
            }
        }
        return false;
    }

}
